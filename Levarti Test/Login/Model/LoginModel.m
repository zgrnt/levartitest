//
//  LoginModel.m
//  Levarti Test
//
//  Created by Zane Grant on 1/2/19.
//  Copyright © 2019 zgrnt. All rights reserved.
//

#import "LoginModel.h"

@implementation LoginModel

static NSString * const usernameConst = @"John";
static NSString * const passwordConst = @"Password";

+ (BOOL)loginSuccessWith:(NSString *)username andPassword:(NSString *)password {
    
    if ([username isEqualToString:usernameConst] && [password isEqualToString:passwordConst]) {
        
        return YES;
    }
    
    return NO;
}

@end
