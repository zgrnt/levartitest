//
//  LoginModel.h
//  Levarti Test
//
//  Created by Zane Grant on 1/2/19.
//  Copyright © 2019 zgrnt. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoginModel : NSObject

+ (BOOL)loginSuccessWith:(NSString *)username andPassword:(NSString *)password;

@end

NS_ASSUME_NONNULL_END
