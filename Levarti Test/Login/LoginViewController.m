//
//  LoginViewController.m
//  Levarti Test
//
//  Created by Zane Grant on 31/1/19.
//  Copyright © 2019 zgrnt. All rights reserved.
//

#import "LoginViewController.h"
#import "LoginModel.h"
#import "LoginButton.h"


@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet LoginButton *loginButton;
@property(nonatomic, strong) LoginModel *model;

@end

@implementation LoginViewController

static NSString * const segueId = @"ResultTVCSegue";

#pragma mark - Button

- (IBAction)loginButtonPressed:(UIButton *)sender {
    
    if (![LoginModel loginSuccessWith:self.nameTextField.text andPassword:self.passwordTextField.text]) {
        
        [self loginError];
    }
}

- (void)loginError {
 
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Login Error" message:@"Please check you username and password" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:nil];
    [controller addAction:action];
    
    [self presentViewController:controller animated:YES completion:nil];
}

@end
