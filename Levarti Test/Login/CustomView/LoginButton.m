//
//  LoginButton.m
//  Levarti Test
//
//  Created by Zane Grant on 2/2/19.
//  Copyright © 2019 zgrnt. All rights reserved.
//

#import "LoginButton.h"
#import "UIButton+ButtonDesign.h"

@implementation LoginButton

- (void)awakeFromNib {
    [super awakeFromNib];
 
    [self setRoundedCorners];
}

@end
