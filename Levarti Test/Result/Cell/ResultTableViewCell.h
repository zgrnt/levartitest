//
//  ResultTableViewCell.h
//  Levarti Test
//
//  Created by Zane Grant on 31/1/19.
//  Copyright © 2019 zgrnt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Photos.h"

NS_ASSUME_NONNULL_BEGIN

@interface ResultTableViewCell : UITableViewCell

- (void)configCell:(Photos *)photo;

@end

NS_ASSUME_NONNULL_END
