//
//  ResultTableViewCell.m
//  Levarti Test
//
//  Created by Zane Grant on 31/1/19.
//  Copyright © 2019 zgrnt. All rights reserved.
//

#import "ResultTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import <EXTScope.h>

@interface ResultTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewThumb;

@end

@implementation ResultTableViewCell

- (void)configCell:(Photos *)photo {
    
    self.titleLabel.text = photo.title;
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:photo.thumbnailUrl]];
    
   @weakify(self);
    [self.imageViewThumb setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        
        @strongify(self)
        self.imageViewThumb.image = image;
        [self blurEffect];
        
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
        
        //Handle error
    }];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    for (UIView *sub in self.imageViewThumb.subviews) {
        
        if ([sub isKindOfClass:[UIVisualEffectView class]]) {
            
            [sub removeFromSuperview];
            break;
        }
    }
}

- (void)blurEffect {
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];

    blurEffectView.frame = self.imageViewThumb.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self.imageViewThumb addSubview:blurEffectView];
}

@end
