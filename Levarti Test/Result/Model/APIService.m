//
//  APIService.m
//  Levarti Test
//
//  Created by Zane Grant on 31/1/19.
//  Copyright © 2019 zgrnt. All rights reserved.
//

#import "APIService.h"
#import <AFNetworking.h>

@implementation APIService

+ (void)fetchJsonPayload:(void (^)(NSArray *))completion {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"https://jsonplaceholder.typicode.com/photos" parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        completion(responseObject);
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

@end
