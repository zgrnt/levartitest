//
//  Photos.h
//  Levarti Test
//
//  Created by Zane Grant on 31/1/19.
//  Copyright © 2019 zgrnt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm.h>

NS_ASSUME_NONNULL_BEGIN

@interface Photos : RLMObject

@property(nonatomic, assign) int albumId;
@property(nonatomic, assign) int id;
@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *url;
@property(nonatomic, copy) NSString *thumbnailUrl;
@property(nonatomic, assign) BOOL deleted;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end

RLM_ARRAY_TYPE(Photos)

NS_ASSUME_NONNULL_END
