//
//  APIService.h
//  Levarti Test
//
//  Created by Zane Grant on 31/1/19.
//  Copyright © 2019 zgrnt. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface APIService : NSObject

+ (void)fetchJsonPayload:(void (^)(NSArray *))completion;

@end

NS_ASSUME_NONNULL_END
