//
//  ResultModel.m
//  Levarti Test
//
//  Created by Zane Grant on 31/1/19.
//  Copyright © 2019 zgrnt. All rights reserved.
//

#import "ResultModel.h"
#import <EXTScope.h>

@implementation ResultModel

- (void)mapJsontoModel:(NSArray *)jsonArray {
    
    @weakify(self);
    dispatch_async(dispatch_queue_create("background", 0), ^{
        @autoreleasepool {
            
            for (NSDictionary *dict in jsonArray) {
                
                Photos *photo = [[Photos alloc] initWithDictionary:dict];
                RLMRealm *realm = [RLMRealm defaultRealm];
                
                [realm transactionWithBlock:^{
                    
                    @strongify(self)
                    Photos *photos = [self getObjectFromId:photo.id];
                    photo.deleted = photos.deleted;
                    [realm addOrUpdateObject:photo];
                }];
            }
        }
    });
}

- (RLMResults<Photos *> *)getResults {
    
    RLMResults<Photos *> *photos = [Photos objectsWhere:@"deleted == false"];

    return photos;
}

- (RLMResults<Photos *> *)getFilteredResults:(NSString *)searchQuery {
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"title contains[c] %@ && deleted == false", searchQuery];
    RLMResults<Photos *> *photos = [Photos objectsWithPredicate:pred];
    
    return photos;
}

- (Photos *)getObjectFromId:(int)iD {
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"id == %i", iD];
    RLMResults<Photos *> *photos = [Photos objectsWithPredicate:pred];
    
    return photos.firstObject;
}

- (void)deleteResultAtRow:(NSInteger)row {
    
    Photos *photo = [self getResults][row];
    [self updateDeletedObject:photo];
}

- (void)deleteFilteredResult:(NSString *)searchQuery AtRow:(NSInteger)row {
    
    Photos *photo = [self getFilteredResults:searchQuery][row];
    [self updateDeletedObject:photo];
}

-(void)updateDeletedObject:(Photos *)photo {
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    photo.deleted = true;
    [realm commitWriteTransaction];
}

@end
