//
//  ResultModel.h
//  Levarti Test
//
//  Created by Zane Grant on 31/1/19.
//  Copyright © 2019 zgrnt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Photos.h"
#import <Realm.h>

NS_ASSUME_NONNULL_BEGIN

@interface ResultModel : NSObject

- (void)mapJsontoModel:(NSArray *)jsonArray;
- (RLMResults<Photos *> *)getResults;
- (RLMResults<Photos *> *)getFilteredResults:(NSString *)searchQuery;
- (void)deleteResultAtRow:(NSInteger)row;
- (void)deleteFilteredResult:(NSString *)searchQuery AtRow:(NSInteger)row;

@end

NS_ASSUME_NONNULL_END
