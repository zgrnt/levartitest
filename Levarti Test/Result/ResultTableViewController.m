//
//  ResultTableViewController.m
//  Levarti Test
//
//  Created by Zane Grant on 31/1/19.
//  Copyright © 2019 zgrnt. All rights reserved.
//

#import "ResultTableViewController.h"
#import "APIService.h"
#import "ResultModel.h"
#import "Photos.h"
#import "ResultTableViewCell.h"
#import <EXTScope.h>

@interface ResultTableViewController () <UISearchBarDelegate, UISearchResultsUpdating, UISearchControllerDelegate>

@property(nonatomic, strong) ResultModel *model;
@property (nonatomic, strong) UISearchController *searchController;

@end

@implementation ResultTableViewController

static NSString * const cellId = @"ResultCell";

- (ResultModel *) model {
    
    if( !_model) {
        
        _model = [[ResultModel alloc] init];
    }
    
    return _model;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configSeachController];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ResultTableViewCell" bundle:nil] forCellReuseIdentifier:cellId];
    
    [self configRefreshControl];
    [self.tableView.refreshControl beginRefreshing];
    [self fetchData];
}

#pragma mark - Fetch Data

- (void)configRefreshControl {
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(fetchData) forControlEvents:UIControlEventValueChanged];
    
    self.tableView.refreshControl = refreshControl;
}

- (void)fetchData {
    
    @weakify(self)
    [APIService fetchJsonPayload:^(NSArray *objects) {
        
        @strongify(self)
        [self.model mapJsontoModel:objects];
        [self.tableView.refreshControl endRefreshing];
        [self.tableView reloadData];
    }];
}

#pragma mark - Table view data source and delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.searchController.active && ![self.searchController.searchBar.text isEqualToString:@""]) {
        
        return [self.model getFilteredResults: self.searchController.searchBar.text].count;
    }
    
    return [self.model getResults].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ResultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    
    if (self.searchController.active && ![self.searchController.searchBar.text isEqualToString:@""]) {

        RLMResults<Photos *> *photos =  [self.model getFilteredResults: self.searchController.searchBar.text];
        [cell configCell:photos[indexPath.row]];
        
    } else {
        
        RLMResults<Photos *> *photos =  [self.model getResults];
        [cell configCell:photos[indexPath.row]];
    }
    
    return cell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        @weakify(self)
        dispatch_async(dispatch_queue_create("background", 0), ^{
            @autoreleasepool {
                
                @strongify(self)
                if (self.searchController.active && ![self.searchController.searchBar.text isEqualToString:@""]) {

                    [self.model deleteFilteredResult:self.searchController.searchBar.text AtRow:indexPath.row];
                    
                } else {
                    
                    [self.model deleteResultAtRow:indexPath.row];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [tableView reloadData];
                });
            }
        });
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 80.0;
}

#pragma mark - Search Controller

- (void)configSeachController {
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    self.searchController.delegate = self;
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    [self.tableView reloadData];
}

@end
