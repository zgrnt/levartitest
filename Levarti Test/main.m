//
//  main.m
//  Levarti Test
//
//  Created by Zane Grant on 31/1/19.
//  Copyright © 2019 zgrnt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
