//
//  UIButton+ButtonDesign.h
//  Levarti Test
//
//  Created by Zane Grant on 2/2/19.
//  Copyright © 2019 zgrnt. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (ButtonDesign)

- (void)setRoundedCorners;

@end

NS_ASSUME_NONNULL_END
