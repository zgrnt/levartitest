//
//  UIButton+ButtonDesign.m
//  Levarti Test
//
//  Created by Zane Grant on 2/2/19.
//  Copyright © 2019 zgrnt. All rights reserved.
//

#import "UIButton+ButtonDesign.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIButton (ButtonDesign)

- (void)setRoundedCorners {
    
    self.layer.cornerRadius = 8;
    self.layer.masksToBounds = true;
}

@end
