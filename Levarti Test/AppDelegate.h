//
//  AppDelegate.h
//  Levarti Test
//
//  Created by Zane Grant on 31/1/19.
//  Copyright © 2019 zgrnt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

